import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        dark: false,
        themes: {
            light: {
                primary: '#1976D2',
                secondary: '#f9f9f9',
                error: '#FF5252',
                success: '#4CAF50',
                warning: '#FFC107',
                game: '#f0f0f0'
            },
            dark: {
                primary: '#3d3d3d',
                secondary: '#ebebeb',
                error: '#FF5252',
                success: '#4CAF50',
                warning: '#FFC107',
                game: '#d1d1d1'
            }
        }
    }
});