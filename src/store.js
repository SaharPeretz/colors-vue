import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        colors: [{
                id: 1,
                name: 'green'
            },
            {
                id: 2,
                name: 'pink'
            },
            {
                id: 3,
                name: 'yellow'
            },
            {
                id: 4,
                name: 'red'
            },
            {
                id: 5,
                name: 'purple'
            },
            {
                id: 6,
                name: 'blue'
            },
            {
                id: 7,
                name: 'brown'
            },
            {
                id: 8,
                name: 'orange'
            }
        ],
        time: "00:00.000",
        generatedSequence: [],
        userGuess: [],
        result: [],
        answers: [],
        attempts: 0,
        isGameDisplayed: false,
        isUserWon: false,
        isShowHomePageErrorMsg: false,
    },
    getters: {
        colors(state) {
            return state.colors;
        },
        generatedSequence(state) {
            return state.generatedSequence;
        },
        userGuess(state) {
            return state.userGuess;
        },
        result(state) {
            return state.result;
        },
        attempts(state) {
            return state.attempts;
        },
        isGameDisplayed(state) {
            return state.isGameDisplayed;
        },
        isUserWon(state) {
            return state.isUserWon;
        },
        isShowHomePageErrorMsg(state) {
            return state.isShowHomePageErrorMsg;
        },
        time(state) {
            return state.time;
        },
    },
    mutations: {
        generateRandomSequence(state) {
            let colors = [...state.colors]
            for (let i = colors.length - 1; i > 0; i--) {
                let randomIndex = Math.floor(Math.random() * (i + 1));
                [colors[i], colors[randomIndex]] = [colors[randomIndex], colors[i]]
            }
            state.generatedSequence = colors.slice(0, 3)
        },
        calcResult(state) {
            let result = [{
                id: 1,
                text: 'total-miss'
            }, {
                id: 2,
                text: 'total-miss'
            }, {
                id: 3,
                text: 'total-miss'
            }];
            for (let i = 0; i < 3; i++) {
                for (let j = 0; j < 3; j++) {
                    if (state.userGuess[i] === state.generatedSequence[i].name) {
                        result[i].text = 'perfect-hit';
                    } else if (state.userGuess[i] === state.generatedSequence[j].name && i !== j) {
                        result[i].text = 'near-hit';
                    }
                }
            }

            state.result = result;

            // Check if user won
            if ((state.result[0].text === 'perfect-hit') && (state.result[1].text === 'perfect-hit') && (state.result[2].text === 'perfect-hit')) {
                state.isUserWon = true;
                state.isTimerRunning = false;
            }
        },
        addAttempt(state) {
            if (state.userGuess.length === 3) {
                state.attempts++;
            } else {
                return;
            }
        },
        resetGuess(state) {
            if (state.userGuess.length === 3 && state.userGuess !== state.generatedSequence) {
                setTimeout(() => {
                    state.userGuess = [];
                    state.result = [];
                }, 1000);
            } else {
                return;
            }
        },
        restartGame(state) {
            this.commit('generateRandomSequence');
            state.userGuess = [];
            state.result = [];
            state.attempts = 0;
            state.isUserWon = false;
            state.time = "00:00.000";
            state.isTimerRunning = false;
        },
        displayGame(state) {
            state.isGameDisplayed = true;
        },
        goHomePage(state) {
            state.isGameDisplayed = false;
        },
        openModal(state) {
            state.isShowModal = true;
        },
        closeModal(state) {
            state.isShowModal = false;
        },
        showHomePageErrorMsg(state) {
            state.isShowHomePageErrorMsg = true;
        },
        dontShowHomePageErrorMsg(state) {
            state.isShowHomePageErrorMsg = false;
        },
        resetTimer(state) {
            state.time = "00:00.000";
        },
        setTimer(state, value) {
            state.time = value;
        },
    },
});

export default store;